package org.demo.web.mda.forms;

import org.demo.vo.CountryVO;
import org.demo.vo.PresidentVO;
import org.demo.web.mda.controllers.Demo01Controller;

import com.vaadin.event.ShortcutAction;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.TextField;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window;

public class NuevoCountry extends Window {

	private static final long serialVersionUID = -7813707094415187188L;

	private VerticalLayout mainLayout, vlDatos;
	private HorizontalLayout hlControles;

	private Button btnSalvar, btnCancelar;
	private TextField tfNombre, tfPresidente;
	private Table tablaProvincias;

	private CountryVO countryVO;
	private PresidentVO presidentVO;
	private Boolean editar = false;

	public NuevoCountry() {
		countryVO = new CountryVO();
		presidentVO = new PresidentVO();
		setCaption("Nuevo Country");
		setResizable(false);

		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);

		buildForm();
		buildControls();

		setContent(mainLayout);
		center();
	}

	public NuevoCountry(CountryVO countryVO, Boolean editar) {
		this.countryVO = countryVO;
		this.presidentVO = countryVO.getPresidentVO();
		this.editar = editar;
		setCaption("Nuevo Country");
		setResizable(false);

		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);

		buildForm();
		buildControls();

		restaurar();

		if (this.editar) {
			setCaption("Editando " + this.countryVO.getName());
		} else {
			setCaption("Visualizando " + this.countryVO.getName());
			tfNombre.setReadOnly(true);
			tfPresidente.setReadOnly(true);
			btnSalvar.setEnabled(false);
		}

		setContent(mainLayout);
		center();
	}

	private void buildForm() {
		vlDatos = new VerticalLayout();
		vlDatos.setSizeFull();
		vlDatos.setSpacing(true);
		vlDatos.setMargin(true);

		tfNombre = new TextField("Nombre:");
		tfNombre.setRequired(true);

		tfPresidente = new TextField("Presidente");
		tfPresidente.setRequired(true);
		
		tablaProvincias = new Table();
		tablaProvincias.setSizeFull();
		tablaProvincias.setSelectable(true);
		tablaProvincias.setImmediate(true);
		tablaProvincias.addContainerProperty("id", Long.class, null, "ID", null, null);
		tablaProvincias.addContainerProperty("nombre", String.class, null, "Nombre", null, null);
		// Esto queda para implementar más adelante

		vlDatos.addComponent(tfNombre);
		vlDatos.addComponent(tfPresidente);

		mainLayout.addComponent(vlDatos);
	}

	private void buildControls() {
		btnSalvar = new Button("Salvar");
		btnSalvar.setClickShortcut(ShortcutAction.KeyCode.ENTER);
		btnSalvar.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -4989055665604141964L;

			@Override
			public void buttonClick(ClickEvent event) {
				if (tfNombre.isValid() && tfPresidente.isValid()) {
					countryVO.setName(tfNombre.getValue());
					presidentVO.setName(tfPresidente.getValue());
					countryVO.setPresidentVO(presidentVO);
					if (editar) {
						Demo01Controller.getInstance().updateCountry(countryVO);
					} else {
						Demo01Controller.getInstance().createCountry(countryVO);
					}
					close();
				}
			}
		});

		btnCancelar = new Button("Cancelar");
		btnCancelar.addClickListener(new ClickListener() {
			private static final long serialVersionUID = -6934902143063642412L;

			@Override
			public void buttonClick(ClickEvent event) {
				close();
			}
		});

		hlControles = new HorizontalLayout();
		hlControles.setSizeFull();
		hlControles.setSpacing(true);
		hlControles.addComponent(btnSalvar);
		hlControles.addComponent(btnCancelar);

		mainLayout.addComponent(hlControles);
	}

	private void restaurar() {
		tfNombre.setValue(countryVO.getName());
		tfPresidente.setValue(presidentVO.getName());
	}
}
