package org.demo.web.mda.controllers;

import java.util.Collection;

import org.demo.ServiceLocator;
import org.demo.vo.CountryVO;

public class Demo01Controller {

	private static Demo01Controller clientController;

	private Demo01Controller() {
		super();
	}

	public static Demo01Controller getInstance() {
		if (clientController == null)
			clientController = new Demo01Controller();
		return clientController;
	}

	@SuppressWarnings("rawtypes")
	public Collection getAllCountries() {
		return ServiceLocator.instance().getCountryService().getAllCountries();
	}

	public Long createCountry(CountryVO countryVO) {
		return ServiceLocator.instance().getCountryService().createCountry(countryVO);
	}

	public void removeCountry(Long idCountry) {
		ServiceLocator.instance().getCountryService().removeCountry(idCountry);
	}

	@SuppressWarnings("rawtypes")
	public Collection getCountryByName(String name) {
		return ServiceLocator.instance().getCountryService().getCountryByName(name);
	}

	public CountryVO getCountryById(Long idCountry) {
		return ServiceLocator.instance().getCountryService().getCountryById(idCountry);
	}

	public Long updateCountry(CountryVO countryVO) {
		return ServiceLocator.instance().getCountryService().updateCountry(countryVO);
	}
}
