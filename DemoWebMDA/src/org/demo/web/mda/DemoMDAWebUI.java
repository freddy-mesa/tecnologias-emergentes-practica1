package org.demo.web.mda;

import java.util.List;

import javax.servlet.annotation.WebServlet;

import org.demo.vo.CountryVO;
import org.demo.web.mda.controllers.Demo01Controller;
import org.demo.web.mda.forms.NuevoCountry;

import com.vaadin.annotations.Theme;
import com.vaadin.annotations.VaadinServletConfiguration;
import com.vaadin.data.Property.ValueChangeEvent;
import com.vaadin.data.Property.ValueChangeListener;
import com.vaadin.server.VaadinRequest;
import com.vaadin.server.VaadinServlet;
import com.vaadin.ui.Button;
import com.vaadin.ui.Button.ClickEvent;
import com.vaadin.ui.Button.ClickListener;
import com.vaadin.ui.HorizontalLayout;
import com.vaadin.ui.Table;
import com.vaadin.ui.UI;
import com.vaadin.ui.VerticalLayout;
import com.vaadin.ui.Window.CloseEvent;
import com.vaadin.ui.Window.CloseListener;

@SuppressWarnings("serial")
@Theme("demowebmda")
public class DemoMDAWebUI extends UI {

	private VerticalLayout mainLayout;
	private HorizontalLayout hlControles;

	private Button btnNuevo, btnEditar, btnRemover, btnVisualizar;
	private Table tablaCountry;

	private CountryVO countrySelected;

	@WebServlet(value = "/*", asyncSupported = true)
	@VaadinServletConfiguration(productionMode = false, ui = DemoMDAWebUI.class)
	public static class Servlet extends VaadinServlet {
	}

	@Override
	protected void init(VaadinRequest request) {
		mainLayout = new VerticalLayout();
		mainLayout.setSpacing(true);
		mainLayout.setMargin(true);

		buildControles();
		buildTable();

		setContent(mainLayout);
	}

	private void buildControles() {
		hlControles = new HorizontalLayout();
		hlControles.setSizeFull();
		hlControles.setSpacing(true);
		hlControles.setMargin(true);

		btnNuevo = new Button("Nuevo Country");
		btnNuevo.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				NuevoCountry nuevoCountry = new NuevoCountry();
				nuevoCountry.addCloseListener(new CloseListener() {
					@Override
					public void windowClose(CloseEvent e) {
						updateTable();
					}
				});
				getUI().addWindow(nuevoCountry);
			}
		});

		btnEditar = new Button("Editar Country");
		btnEditar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (countrySelected != null) {
					NuevoCountry nuevoCountry = new NuevoCountry(countrySelected, true);
					nuevoCountry.addCloseListener(new CloseListener() {
						@Override
						public void windowClose(CloseEvent e) {
							updateTable();
						}
					});
					getUI().addWindow(nuevoCountry);
				}
			}
		});

		btnVisualizar = new Button("Visualizar Country");
		btnVisualizar.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				if (countrySelected != null) {
					NuevoCountry nuevoCountry = new NuevoCountry(countrySelected, false);
					nuevoCountry.addCloseListener(new CloseListener() {
						@Override
						public void windowClose(CloseEvent e) {
							updateTable();
						}
					});
					getUI().addWindow(nuevoCountry);
				}
			}
		});

		btnRemover = new Button("Eliminar Country");
		btnRemover.addClickListener(new ClickListener() {
			@Override
			public void buttonClick(ClickEvent event) {
				Demo01Controller.getInstance().removeCountry(countrySelected.getId());
				updateTable();
			}
		});

		hlControles.addComponent(btnNuevo);
		hlControles.addComponent(btnEditar);
		hlControles.addComponent(btnVisualizar);
		hlControles.addComponent(btnRemover);

		mainLayout.addComponent(hlControles);
	}

	private void buildTable() {
		tablaCountry = new Table();
		tablaCountry.setSizeFull();
		tablaCountry.setSelectable(true);
		tablaCountry.setImmediate(true);
		tablaCountry.addContainerProperty("id", Long.class, null, "ID", null, null);
		tablaCountry.addContainerProperty("nombre", String.class, null, "Nombre", null, null);
		tablaCountry.addContainerProperty("presidenteID", Long.class, null, "Presidente ID", null, null);
		tablaCountry.addContainerProperty("presidenteNombre", String.class, null, "Presidente nombre", null, null);
		tablaCountry.addValueChangeListener(new ValueChangeListener() {
			@Override
			public void valueChange(ValueChangeEvent event) {
				Object valor = tablaCountry.getValue();
				if (valor != null) {
					countrySelected = (CountryVO) valor;
				}
			}
		});
		
		updateTable();

		mainLayout.addComponent(tablaCountry);
	}

	@SuppressWarnings("unchecked")
	private void updateTable() {
		tablaCountry.removeAllItems();
		List<CountryVO> listaCountry = (List<CountryVO>) Demo01Controller.getInstance().getAllCountries();
		for (CountryVO c : listaCountry) {
			CountryVO lleno = Demo01Controller.getInstance().getCountryById(c.getId());
			tablaCountry.addItem(new Object[] { lleno.getId(), lleno.getName(), lleno.getPresidentVO().getId(), lleno.getPresidentVO().getName() }, lleno);
		}
	}

}