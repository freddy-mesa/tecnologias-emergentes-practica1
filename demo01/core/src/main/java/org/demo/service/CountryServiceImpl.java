// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 */
package org.demo.service;

import java.util.ArrayList;
import java.util.Collection;

import org.demo.domain.Country;
import org.demo.domain.CountryDao;
import org.demo.domain.FutbolTeam;
import org.demo.domain.President;
import org.demo.domain.Province;
import org.demo.vo.CountryVO;
import org.demo.vo.PresidentVO;
import org.demo.vo.FutbolTeamVO;
import org.demo.vo.ProvinceVO;

/**
 * @see org.demo.service.CountryService
 */
public class CountryServiceImpl
    extends org.demo.service.CountryServiceBase
{

	@Override
	protected long handleCreateCountry(CountryVO countryVO) 
			throws java.lang.Exception 
	{		
		Long id = null;
		
		//Se crea una instancia de presidente del countryVO
		President president = President.Factory.newInstance();
		getPresidentDao().presidentVOToEntity(countryVO.getPresidentVO(), president, true);
		getPresidentDao().create(president);
		
		//Se crea una instancia en la DB del futbol team de un countryVO
		FutbolTeam futbolTeam = FutbolTeam.Factory.newInstance();
		getFutbolTeamDao().futbolTeamVOToEntity(countryVO.getFutbolTeamVO(), futbolTeam, true);
		getFutbolTeamDao().create(futbolTeam);
		
		//Se crea una instancia de country
		Country country = Country.Factory.newInstance();
		
		//Se inserta el presidente
		country.setPresident(president);
		
		//Se inserta el futbol team
		country.setFutbolTeam(futbolTeam);
		
		getCountryDao().countryVOToEntity(countryVO, country, true);
		
		id = getCountryDao().create(country).getId();
//		country = null;
		
//		country = getCountryDao().load(id);
		
		//Se crea una instancia en la DB de las provincias de un countryVO
//		ArrayList<Province> provinces = null;
//		if(country.getProvinces() != null){
//			provinces = new ArrayList<Province>();
//			ArrayList<Province> allProvince = (ArrayList<Province>) country.getProvinces();
//			 
//			for(Province province : provinces){
//				Province newProvince = Province.Factory.newInstance();
//				getProvinceDao().provinceVOToEntity(provinceVO, newProvince, true);
//				getProvinceDao().create(province);
//				provinces.add(province);
//			}
//		}
		
		//Se inserta las provincias
		/*if(provinces != null)
			country.setProvinces((Collection)provinces);
		*/
		
		return id;
	}

	@Override
	protected Collection handleGetAllCountries() throws Exception {
		return getCountryDao().loadAll(CountryDao.TRANSFORM_COUNTRYVO);
	}

	@Override
	protected Collection handleGetCountryByName(String name) throws Exception {		
		return getCountryDao().loadCountryByName(CountryDao.TRANSFORM_COUNTRYVO, name);
	}

	@Override
	protected CountryVO handleGetCountryById(Long idCountry) throws Exception {
		Country country = getCountryDao().load(idCountry);
		CountryVO countryVO = getCountryDao().toCountryVO(country);
		
		PresidentVO presidentVO = null;
		if(country.getPresident() != null) {
			presidentVO = getPresidentDao().toPresidentVO(country.getPresident());
		}
		countryVO.setPresidentVO(presidentVO);		
		
		FutbolTeamVO futbolTeamVO = null;
		if(country.getFutbolTeam() != null){
			futbolTeamVO = getFutbolTeamDao().toFutbolTeamVO(country.getFutbolTeam());
		}
		countryVO.setFutbolTeamVO(futbolTeamVO);
		
		if(country.getProvinces().size() > 0){
			ArrayList<ProvinceVO> provincesVO = new ArrayList<ProvinceVO>();
			ArrayList<Province> provinces = (ArrayList<Province>) country.getProvinces();
			for(Province province : provinces){
				provincesVO.add(getProvinceDao().toProvinceVO(province));
			}
			
			countryVO.setProvinceVOs((Collection) provincesVO);
		}
				
		return countryVO;
	}

	@Override
	protected Long handleUpdateCountry(CountryVO countryVO) throws Exception {
		Country country = getCountryDao().load(countryVO.getId());
		
		President president = getPresidentDao().load(country.getPresident().getId());
		getPresidentDao().presidentVOToEntity(countryVO.getPresidentVO(), president, true);
		getPresidentDao().update(president);
		
		FutbolTeam futbolTeam = getFutbolTeamDao().load(country.getFutbolTeam().getId());
		getFutbolTeamDao().futbolTeamVOToEntity(countryVO.getFutbolTeamVO(), futbolTeam, true);
		getFutbolTeamDao().update(futbolTeam);
		
		//Se crea una instancia en la DB de las provincias de un countryVO
		/*ArrayList<ProvinceVO> provinceVOs = (ArrayList<ProvinceVO>) countryVO.getProvinceVOs();
		for(ProvinceVO provinceVO : provinceVOs){
			if(provinceVO.getId() == null){
				Province province = Province.Factory.newInstance();
				getProvinceDao().provinceVOToEntity(provinceVO, province, true);
				getProvinceDao().create(province);
			}
			else{
				Province province = getProvinceDao().load(provinceVO.getId());
				getProvinceDao().provinceVOToEntity(provinceVO, province, true);
				getProvinceDao().update(province);
			}
		}*/
				
		getCountryDao().countryVOToEntity(countryVO, country, true);
		getCountryDao().update(country);
		
		return country.getId();
	}

	@Override
	protected void handleRemoveCountry(Long idCountry) throws Exception {
		getCountryDao().remove(idCountry);
	}
	
	protected FutbolTeamVO handleGetFutbolTeamByCountry(CountryVO countryVO) {
		FutbolTeam futbolTeam = getFutbolTeamDao().load(countryVO.getFutbolTeamVO().getId());
		FutbolTeamVO futbolTeamVO = getFutbolTeamDao().toFutbolTeamVO(futbolTeam);
		
		return futbolTeamVO;
	}

	@Override
	protected Collection handleGetProvincesByCountry(CountryVO countryVO)
			throws Exception {
		return getCountryDao().load(countryVO.getId()).getProvinces();
	}
    
}