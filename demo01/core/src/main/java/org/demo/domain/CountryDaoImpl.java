// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 */
package org.demo.domain;
/**
 * @see org.demo.domain.Country
 */
public class CountryDaoImpl
    extends org.demo.domain.CountryDaoBase
{
    /**
     * @see org.demo.domain.CountryDao#toCountryVO(org.demo.domain.Country, org.demo.vo.CountryVO)
     */
    public void toCountryVO(
        org.demo.domain.Country source,
        org.demo.vo.CountryVO target)
    {
        // @todo verify behavior of toCountryVO
        super.toCountryVO(source, target);
    }


    /**
     * @see org.demo.domain.CountryDao#toCountryVO(org.demo.domain.Country)
     */
    public org.demo.vo.CountryVO toCountryVO(final org.demo.domain.Country entity)
    {
        // @todo verify behavior of toCountryVO
        return super.toCountryVO(entity);
    }


    /**
     * Retrieves the entity object that is associated with the specified value object
     * from the object store. If no such entity object exists in the object store,
     * a new, blank entity is created
     */
    private org.demo.domain.Country loadCountryFromCountryVO(org.demo.vo.CountryVO countryVO)
    {
        // @todo implement loadCountryFromCountryVO
        throw new java.lang.UnsupportedOperationException("org.demo.domain.loadCountryFromCountryVO(org.demo.vo.CountryVO) not yet implemented.");

        /* A typical implementation looks like this:
        org.demo.domain.Country country = this.load(countryVO.getId());
        if (country == null)
        {
            country = org.demo.domain.Country.Factory.newInstance();
        }
        return country;
        */
    }

    
    /**
     * @see org.demo.domain.CountryDao#countryVOToEntity(org.demo.vo.CountryVO)
     */
    public org.demo.domain.Country countryVOToEntity(org.demo.vo.CountryVO countryVO)
    {
        // @todo verify behavior of countryVOToEntity
        org.demo.domain.Country entity = this.loadCountryFromCountryVO(countryVO);
        this.countryVOToEntity(countryVO, entity, true);
        return entity;
    }


    /**
     * @see org.demo.domain.CountryDao#countryVOToEntity(org.demo.vo.CountryVO, org.demo.domain.Country)
     */
    public void countryVOToEntity(
        org.demo.vo.CountryVO source,
        org.demo.domain.Country target,
        boolean copyIfNull)
    {
        // @todo verify behavior of countryVOToEntity
        super.countryVOToEntity(source, target, copyIfNull);
    }

}