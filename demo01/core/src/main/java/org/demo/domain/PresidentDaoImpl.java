// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 */
package org.demo.domain;
/**
 * @see org.demo.domain.President
 */
public class PresidentDaoImpl
    extends org.demo.domain.PresidentDaoBase
{
    /**
     * @see org.demo.domain.PresidentDao#toPresidentVO(org.demo.domain.President, org.demo.vo.PresidentVO)
     */
    public void toPresidentVO(
        org.demo.domain.President source,
        org.demo.vo.PresidentVO target)
    {
        // @todo verify behavior of toPresidentVO
        super.toPresidentVO(source, target);
    }


    /**
     * @see org.demo.domain.PresidentDao#toPresidentVO(org.demo.domain.President)
     */
    public org.demo.vo.PresidentVO toPresidentVO(final org.demo.domain.President entity)
    {
        // @todo verify behavior of toPresidentVO
        return super.toPresidentVO(entity);
    }


    /**
     * Retrieves the entity object that is associated with the specified value object
     * from the object store. If no such entity object exists in the object store,
     * a new, blank entity is created
     */
    private org.demo.domain.President loadPresidentFromPresidentVO(org.demo.vo.PresidentVO presidentVO)
    {
        // @todo implement loadPresidentFromPresidentVO
        throw new java.lang.UnsupportedOperationException("org.demo.domain.loadPresidentFromPresidentVO(org.demo.vo.PresidentVO) not yet implemented.");

        /* A typical implementation looks like this:
        org.demo.domain.President president = this.load(presidentVO.getId());
        if (president == null)
        {
            president = org.demo.domain.President.Factory.newInstance();
        }
        return president;
        */
    }

    
    /**
     * @see org.demo.domain.PresidentDao#presidentVOToEntity(org.demo.vo.PresidentVO)
     */
    public org.demo.domain.President presidentVOToEntity(org.demo.vo.PresidentVO presidentVO)
    {
        // @todo verify behavior of presidentVOToEntity
        org.demo.domain.President entity = this.loadPresidentFromPresidentVO(presidentVO);
        this.presidentVOToEntity(presidentVO, entity, true);
        return entity;
    }


    /**
     * @see org.demo.domain.PresidentDao#presidentVOToEntity(org.demo.vo.PresidentVO, org.demo.domain.President)
     */
    public void presidentVOToEntity(
        org.demo.vo.PresidentVO source,
        org.demo.domain.President target,
        boolean copyIfNull)
    {
        // @todo verify behavior of presidentVOToEntity
        super.presidentVOToEntity(source, target, copyIfNull);
    }

}