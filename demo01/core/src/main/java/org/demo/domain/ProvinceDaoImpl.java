// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 */
package org.demo.domain;
/**
 * @see org.demo.domain.Province
 */
public class ProvinceDaoImpl
    extends org.demo.domain.ProvinceDaoBase
{
    /**
     * @see org.demo.domain.ProvinceDao#toProvinceVO(org.demo.domain.Province, org.demo.vo.ProvinceVO)
     */
    public void toProvinceVO(
        org.demo.domain.Province source,
        org.demo.vo.ProvinceVO target)
    {
        // @todo verify behavior of toProvinceVO
        super.toProvinceVO(source, target);
    }


    /**
     * @see org.demo.domain.ProvinceDao#toProvinceVO(org.demo.domain.Province)
     */
    public org.demo.vo.ProvinceVO toProvinceVO(final org.demo.domain.Province entity)
    {
        // @todo verify behavior of toProvinceVO
        return super.toProvinceVO(entity);
    }


    /**
     * Retrieves the entity object that is associated with the specified value object
     * from the object store. If no such entity object exists in the object store,
     * a new, blank entity is created
     */
    private org.demo.domain.Province loadProvinceFromProvinceVO(org.demo.vo.ProvinceVO provinceVO)
    {
        // @todo implement loadProvinceFromProvinceVO
        throw new java.lang.UnsupportedOperationException("org.demo.domain.loadProvinceFromProvinceVO(org.demo.vo.ProvinceVO) not yet implemented.");

        /* A typical implementation looks like this:
        org.demo.domain.Province province = this.load(provinceVO.getId());
        if (province == null)
        {
            province = org.demo.domain.Province.Factory.newInstance();
        }
        return province;
        */
    }

    
    /**
     * @see org.demo.domain.ProvinceDao#provinceVOToEntity(org.demo.vo.ProvinceVO)
     */
    public org.demo.domain.Province provinceVOToEntity(org.demo.vo.ProvinceVO provinceVO)
    {
        // @todo verify behavior of provinceVOToEntity
        org.demo.domain.Province entity = this.loadProvinceFromProvinceVO(provinceVO);
        this.provinceVOToEntity(provinceVO, entity, true);
        return entity;
    }


    /**
     * @see org.demo.domain.ProvinceDao#provinceVOToEntity(org.demo.vo.ProvinceVO, org.demo.domain.Province)
     */
    public void provinceVOToEntity(
        org.demo.vo.ProvinceVO source,
        org.demo.domain.Province target,
        boolean copyIfNull)
    {
        // @todo verify behavior of provinceVOToEntity
        super.provinceVOToEntity(source, target, copyIfNull);
    }

}