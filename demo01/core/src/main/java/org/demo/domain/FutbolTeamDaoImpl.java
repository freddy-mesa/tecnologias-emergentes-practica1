// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 */
package org.demo.domain;
/**
 * @see org.demo.domain.FutbolTeam
 */
public class FutbolTeamDaoImpl
    extends org.demo.domain.FutbolTeamDaoBase
{
    /**
     * @see org.demo.domain.FutbolTeamDao#toFutbolTeamVO(org.demo.domain.FutbolTeam, org.demo.vo.FutbolTeamVO)
     */
    public void toFutbolTeamVO(
        org.demo.domain.FutbolTeam source,
        org.demo.vo.FutbolTeamVO target)
    {
        // @todo verify behavior of toFutbolTeamVO
        super.toFutbolTeamVO(source, target);
    }


    /**
     * @see org.demo.domain.FutbolTeamDao#toFutbolTeamVO(org.demo.domain.FutbolTeam)
     */
    public org.demo.vo.FutbolTeamVO toFutbolTeamVO(final org.demo.domain.FutbolTeam entity)
    {
        // @todo verify behavior of toFutbolTeamVO
        return super.toFutbolTeamVO(entity);
    }


    /**
     * Retrieves the entity object that is associated with the specified value object
     * from the object store. If no such entity object exists in the object store,
     * a new, blank entity is created
     */
    private org.demo.domain.FutbolTeam loadFutbolTeamFromFutbolTeamVO(org.demo.vo.FutbolTeamVO futbolTeamVO)
    {
        // @todo implement loadFutbolTeamFromFutbolTeamVO
        throw new java.lang.UnsupportedOperationException("org.demo.domain.loadFutbolTeamFromFutbolTeamVO(org.demo.vo.FutbolTeamVO) not yet implemented.");

        /* A typical implementation looks like this:
        org.demo.domain.FutbolTeam futbolTeam = this.load(futbolTeamVO.getId());
        if (futbolTeam == null)
        {
            futbolTeam = org.demo.domain.FutbolTeam.Factory.newInstance();
        }
        return futbolTeam;
        */
    }

    
    /**
     * @see org.demo.domain.FutbolTeamDao#futbolTeamVOToEntity(org.demo.vo.FutbolTeamVO)
     */
    public org.demo.domain.FutbolTeam futbolTeamVOToEntity(org.demo.vo.FutbolTeamVO futbolTeamVO)
    {
        // @todo verify behavior of futbolTeamVOToEntity
        org.demo.domain.FutbolTeam entity = this.loadFutbolTeamFromFutbolTeamVO(futbolTeamVO);
        this.futbolTeamVOToEntity(futbolTeamVO, entity, true);
        return entity;
    }


    /**
     * @see org.demo.domain.FutbolTeamDao#futbolTeamVOToEntity(org.demo.vo.FutbolTeamVO, org.demo.domain.FutbolTeam)
     */
    public void futbolTeamVOToEntity(
        org.demo.vo.FutbolTeamVO source,
        org.demo.domain.FutbolTeam target,
        boolean copyIfNull)
    {
        // @todo verify behavior of futbolTeamVOToEntity
        super.futbolTeamVOToEntity(source, target, copyIfNull);
    }

}