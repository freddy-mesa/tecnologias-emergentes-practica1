package org.pucmm.velocity.test;

import java.net.URL;

public class Main {
	// Enlace
	// http://www.javaworld.com/article/2075966/core-java/start-up-the-velocity-template-engine.html

	// getClass().getClassLoader().getResource(); devuelve una URL
	// Map map = new HashMap(); Para listas complejas

	public static void main(String[] args) {
		VelocityTest vt = new VelocityTest();

		// Check if file exist
		URL url = Main.class.getClassLoader().getResource("resources/VentanaPrincipal.vm");
		System.out.println(url.getFile().toString());

		// Set template to load
		vt.setTemplate("resources/VentanaPrincipal.vm");

		// Add parameter to replace on
		vt.addParameter("class", "Hola");
		vt.addParameter("package", "org.demo.vaadin.test");
		vt.doWork();

		// Show result.
		System.out.println(vt.getResult());
	}

	// Example on list
	/*
	 * $petList.size() Pets on Sale! We are proud to offer these fine pets at
	 * these amazing prices. This month only, choose from: #foreach( $pet in
	 * $petList ) $pet.name for only $pet.price #end Call Today!
	 * 
	 * // create our list of maps ArrayList list = new ArrayList(); Map map =
	 * new HashMap(); map.put("name", "horse"); map.put("price", "00.00");
	 * list.add( map );
	 * 
	 * map = new HashMap(); map.put("name", "dog"); map.put("price", "9.99");
	 * list.add( map ); map = new HashMap(); map.put("name", "bear");
	 * map.put("price", ".99"); list.add( map );
	 */

}
