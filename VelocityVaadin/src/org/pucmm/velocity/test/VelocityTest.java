package org.pucmm.velocity.test;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.StringWriter;
import java.util.List;

import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

public class VelocityTest {

	private VelocityEngine ve;
	private VelocityContext context;
	private Template t;
	private StringWriter writer;

	// private String newline = System.getProperty("line.separator");
	private BufferedWriter bufferWriter;

	public VelocityTest() {
		ve = new VelocityEngine();
		context = new VelocityContext();
		writer = new StringWriter();

		ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
		ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
		ve.init();
	}

	/**
	 * Set template to use.
	 */
	public void setTemplate(String template) {
		t = ve.getTemplate(template);
	}

	/**
	 * Add value to parameter.
	 */
	public void addParameter(String name, String value) {
		context.put(name, value);
	}

	/**
	 * Add List to parameter.
	 */
	public void addParameter(String name, List<Object> value) {
		context.put(name, value);
	}

	/**
	 * Method to execute the merge.
	 */
	public void doWork() {
		t.merge(context, writer);
	}

	/**
	 * Method for get String of result.
	 */
	public String getResult() {
		return writer.toString();
	}

	/**
	 * Method for save to a file
	 * 
	 * @return Boolean true when is successful
	 */
	public boolean saveToFile(String file) {
		try {
			File save = new File(file);
			bufferWriter = new BufferedWriter(new FileWriter(save));
			bufferWriter.write(writer.toString());
		} catch (Exception e) {
			return false;
		}
		return true;
	}

}
