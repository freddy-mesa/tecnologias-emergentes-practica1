/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.client.controller;

import com.client.model.CountryBeanVO;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.TextField;
import javafx.stage.Stage;

/**
 *
 * @author Freddy Mesa
 */
public class ActivityCreateUpdateController implements Initializable {
    private Stage dialogState;
    private boolean isCreate;
    private CountryBeanVO countryBeanVO;
    
    @FXML TextField countryName;
    @FXML TextField presidentName;
    @FXML TextField futbolTeamName;
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        
    }
    
    public void setDialogState(Stage dialogStage) {
        this.dialogState = dialogStage;
    }

    public void setIsCreate(boolean isCreate) {
        this.isCreate = isCreate;
    }

    public void init(CountryBeanVO countryBeanVO) {
        
        this.countryBeanVO = countryBeanVO;
        
        if(isCreate)
            this.showCountryBeanVOData(null);
        else
            this.showCountryBeanVOData(countryBeanVO);
    }
    
    private void showCountryBeanVOData(CountryBeanVO countryBeanVO){
        if(countryBeanVO != null){
            this.countryName.setText(countryBeanVO.getCountryName());
            this.presidentName.setText(countryBeanVO.getPresidentName());
            this.futbolTeamName.setText(countryBeanVO.getFutbolTeamName());
        }
        else {
            this.countryName.setText("");
            this.presidentName.setText("");
            this.futbolTeamName.setText("");
        }
    }
    
    @FXML private void handleButtonSave(){
        if(isCreate){
            new CountryBeanVO(
                    countryName.getText(),
                    presidentName.getText(),
                    futbolTeamName.getText()
            ).createCountryVO();
        }
        else {
            CountryBeanVO updateCountryBeanVO = new CountryBeanVO(
                    countryName.getText(),
                    presidentName.getText(),
                    futbolTeamName.getText()
            );
            
            updateCountryBeanVO.setCountryId(this.countryBeanVO.getCountryId());
            updateCountryBeanVO.updateCountryVO();            
        }
        
        this.dialogState.close();
    }
    
    @FXML private void handleButtonCancel(){
        this.dialogState.close();
    }
}