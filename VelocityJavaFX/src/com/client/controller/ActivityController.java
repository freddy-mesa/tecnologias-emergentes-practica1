package com.client.controller;

import com.client.MainApp;
import com.client.model.ClientController;
import com.client.model.CountryBeanVO;
import java.net.URL;
import java.util.List;
import java.util.ResourceBundle;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Dialogs;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;

import org.demo.vo.CountryVO;

/**
 *
 * @author Freddy Mesa
 */
public class ActivityController implements Initializable {
    
    private MainApp mainApp;
    private boolean isSelected;
    
    @FXML TableView<CountryBeanVO> activityTable;
    @FXML TableColumn<CountryBeanVO, String> countryColumn;
    @FXML TableColumn<CountryBeanVO, String> presidentColumn;
    @FXML TableColumn<CountryBeanVO, String> futbolTeamColumn;
    
    CountryBeanVO countryBeanVO = new CountryBeanVO();
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        // TODO
        countryColumn.setCellValueFactory(
            new PropertyValueFactory<CountryBeanVO, String>("countryName")
        );
        
        presidentColumn.setCellValueFactory(
            new PropertyValueFactory<CountryBeanVO, String>("presidentName")
        );
        
        futbolTeamColumn.setCellValueFactory(
            new PropertyValueFactory<CountryBeanVO, String>("futbolTeamName")
        );
        
        activityTable.getSelectionModel().selectedItemProperty().addListener(
            new ChangeListener<CountryBeanVO>(){
                @Override
                public void changed(ObservableValue<? extends CountryBeanVO> observableList, 
                                    CountryBeanVO oldValue, 
                                    CountryBeanVO newValue) {
                    countryBeanVO = newValue;
                    isSelected = true;
                }
            }
        );
        
        updateTableView();
    }
    
    private void updateTableView(){
        activityTable.setItems(null);
        
        List<CountryVO> countryVOs = (List<CountryVO>)ClientController.getInstance().getAllCountries();
        ObservableList<CountryBeanVO> data = FXCollections.observableArrayList();
        for(CountryVO countryVO : countryVOs){
            CountryVO countryVOLoad = ClientController.getInstance().getCountryById(countryVO.getId());
            data.add(new CountryBeanVO(countryVOLoad));
        }
        
        activityTable.setItems(data);
    }
    
    @FXML private void handleButtonCreateCountry(){
        
        this.mainApp.showCreateEditCountry(true,countryBeanVO);
        this.updateTableView();
    }
    
    @FXML private void handleButtonUpdateCountry(){
        if(isSelected){
            this.mainApp.showCreateEditCountry(false,countryBeanVO);
            this.updateTableView();
        }
        else {
            Dialogs.showErrorDialog(this.mainApp.getPrimaryStage(), "Debe seleccionar un Item de la Lista");
        }
    }
    
    @FXML private void handleButtonRemoveCountry(){
        if(isSelected){
            this.countryBeanVO.removeCountryVO();
            this.updateTableView();
        }
        else {
            Dialogs.showErrorDialog(this.mainApp.getPrimaryStage(), "Debe seleccionar un Item de la Lista");
        }
    }

    public MainApp getMainApp() {
        return mainApp;
    }
    
    public void setMainApp(MainApp mainApp ) {
        this.mainApp = mainApp;
    }
}
