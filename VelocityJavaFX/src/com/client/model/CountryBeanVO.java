package com.client.model;

import org.demo.vo.CountryVO;
import org.demo.vo.FutbolTeamVO;
import org.demo.vo.PresidentVO;

/**
 *
 * @author Freddy Mesa
 */
public class CountryBeanVO {
    private String countryName;
    private Long countryId;
    private String presidentName;
    private String futbolTeamName;
    
    public CountryBeanVO() {
    
    }
    
    public CountryBeanVO(CountryVO countryVO) {
        this.countryId = countryVO.getId();
        this.countryName = countryVO.getName();
        if(countryVO.getPresidentVO() != null){
            this.presidentName = countryVO.getPresidentVO().getName();
        }
        if(countryVO.getFutbolTeamVO() != null){
            this.futbolTeamName = countryVO.getFutbolTeamVO().getName();
        }
    }
    
    public CountryBeanVO(String countryName,
                         String presidentName,
                         String futbolTeamName)
    {
        this.countryName = countryName;
        this.presidentName = presidentName;
        this.futbolTeamName = futbolTeamName;
    }
    
    /**
     * @return the countryName
     */
    public String getCountryName() {
        return countryName;
    }

    /**
     * @param countryName the countryName to set
     */
    public void setCountryName(String countryName) {
        this.countryName = countryName;
    }

    /**
     * @return the presidentName
     */
    public String getPresidentName() {
        return presidentName;
    }

    /**
     * @param presidentName the presidentName to set
     */
    public void setPresidentName(String presidentName) {
        this.presidentName = presidentName;
    }

    /**
     * @return the futbolTeamName
     */
    public String getFutbolTeamName() {
        return futbolTeamName;
    }

    /**
     * @param futbolTeamName the futbolTeamName to set
     */
    public void setFutbolTeamName(String futbolTeamName) {
        this.futbolTeamName = futbolTeamName;
    }

    public Long getCountryId() {
        return countryId;
    }

    public void setCountryId(Long countryId) {
        this.countryId = countryId;
    }
    
    public void createCountryVO(){
        PresidentVO presidentVO = new PresidentVO();
        presidentVO.setName(this.getPresidentName());

        FutbolTeamVO futbolTeamVO = new FutbolTeamVO();
        futbolTeamVO.setName(this.getFutbolTeamName());

        CountryVO countryVO = new CountryVO();
        countryVO.setName(this.getCountryName());

        countryVO.setPresidentVO(presidentVO);
        countryVO.setFutbolTeamVO(futbolTeamVO);
        
        ClientController.getInstance().createCountry(countryVO);
}
    
    public void updateCountryVO(){
        CountryVO countryVO = ClientController.getInstance().
                getCountryById(this.getCountryId());
        
        countryVO.getPresidentVO().setName(this.getPresidentName());        
        countryVO.getFutbolTeamVO().setName(this.getFutbolTeamName());
        countryVO.setName(this.getCountryName());
        
        ClientController.getInstance().updateCountry(countryVO);
    }
    
    public void removeCountryVO(){
        CountryVO countryVO = ClientController.getInstance().
                getCountryById(this.getCountryId());
        
        ClientController.getInstance().removeCountry(countryVO.getId());
    }
}
