/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.client;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.util.List;
import java.util.Map;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 *
 * @author Freddy Mesa
 */
public class VelocityBuilder {

    private final VelocityEngine ve;
    private final VelocityContext context;
    private Template t;
    private final StringWriter writer;
    private final String filePath;
    private boolean isCreated;

    // private String newline = System.getProperty("line.separator");
    private BufferedWriter bufferWriter;

    /**
     *
     * @param filePath
     */
    public VelocityBuilder(String filePath) {
        this.filePath = filePath;
        ve = new VelocityEngine();
        context = new VelocityContext();
        writer = new StringWriter();

        ve.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        ve.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        ve.init();
        
        isCreated = new File(filePath).exists();
    }
    
    public boolean getIsCreated(){
        return isCreated;
    }

    /**
     * Set template to use.
     *
     * @param template
     */
    public void setTemplate(String template) {
        t = ve.getTemplate(template);
    }

    /**
     * Add value to parameter.
     *
     * @param name
     * @param value
     */
    public void addParameter(String name, Object value) {
        context.put(name, value);
    }

    /**
     * Add List to parameter.
     *
     * @param name
     * @param value
     */
    public void addParameter(String name, List<Object> value) {
        context.put(name, value);
    }

    /**
     * Add Map to parameter
     *
     * @param name
     * @param value
     */
    public void addParameter(String name, Map value) {
        context.put(name, value);
    }

    /**
     * Method to execute the merge.
     */
    public void doWork() {
        t.merge(context, writer);
    }

    /**
     * Method for get String of result.
     *
     * @return
     */
    public String getResult() {
        return writer.toString();
    }

    /**
     * Method for save to a file
     *
     * @return Boolean true when is successful
     */
    public boolean saveToFile() {
        try {
            File save = new File(filePath);
            if (!save.exists()) {
                save.createNewFile();
            }
            bufferWriter = new BufferedWriter(new FileWriter(save));
            bufferWriter.write(writer.toString());
            bufferWriter.close();
        } catch (IOException ex) {
            ex.printStackTrace();
            return false;
        }
        return true;
    }
}
