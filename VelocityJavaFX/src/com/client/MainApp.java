/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.client;

import com.client.controller.ActivityController;
import com.client.controller.ActivityCreateUpdateController;
import com.client.model.CountryBeanVO;
import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Freddy Mesa
 */
public class MainApp extends Application {

    Stage primaryStage;
    BorderPane rootLayout;

    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        primaryStage.setTitle("Country CRUD");

        rootLayout = FXMLLoader.load(getClass().getResource("view/RootLayout.fxml"));

        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);

        stage.show();

        showMainWindow();
    }

    public void showMainWindow() throws Exception {
        VelocityBuilder vt = new VelocityBuilder("./src/com/client/view/ActivityView.fxml");

        if (!vt.getIsCreated()) {
            createMainWindow(vt);
        }

        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ActivityView.fxml"));
        AnchorPane page = (AnchorPane) loader.load();
        rootLayout.setCenter(page);

        ActivityController controller = loader.getController();
        controller.setMainApp(this);
    }

    public void showCreateEditCountry(boolean isCreate, CountryBeanVO countryBeanVO){
        try {
            
            VelocityBuilder vt = new VelocityBuilder("./src/com/client/view/ActivityCreateUpdateView.fxml");

            if (!vt.getIsCreated()) {
                createUpdateCountry(vt);
            }
            
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/ActivityCreateUpdateView.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            Stage dialogStage = new Stage();
            
            if(isCreate)
                dialogStage.setTitle("Crear un Pais");
            else
                dialogStage.setTitle("Editar un Pais");
                
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            
            ActivityCreateUpdateController controller = loader.getController();
            controller.setIsCreate(isCreate);
            controller.setDialogState(dialogStage);
            
            controller.init(countryBeanVO);          
            
            dialogStage.showAndWait();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }        
    }
    
    private void createMainWindow(VelocityBuilder vt) {
        // Check if file exist
        URL url = MainApp.class.getClassLoader().getResource("resources/ActivityView.vm");
        System.out.println(url.getFile().toString());

        // Set template to load
        vt.setTemplate("resources/ActivityView.vm");

        // Add parameter to replace on
        ArrayList list = new ArrayList();
        Map map = new HashMap();

        map.put("name", "AnchorPane");
        map.put("controller", "com.client.controller.ActivityController");
        vt.addParameter("class", new HashMap(map));

        map.clear();
        map.put("id", "activityTable");
        vt.addParameter("table", new HashMap(map));

        map.clear();
        map.put("id", "countryColumn");
        map.put("textName", "Pais");
        map.put("width", "186.0");
        list.add(new HashMap(map));

        map.clear();
        map.put("id", "presidentColumn");
        map.put("textName", "Presidente");
        map.put("width", "150.0");
        list.add(new HashMap(map));

        map.clear();
        map.put("id", "futbolTeamColumn");
        map.put("textName", "Equipo de Futbol");
        map.put("width", "150.0");
        list.add(new HashMap(map));

        vt.addParameter("columnList", new ArrayList(list));

        list.clear();

        map.clear();
        map.put("textName", "Crear Pais");
        map.put("handleMethodName", "handleButtonCreateCountry");
        map.put("layoutX", "14.0");
        list.add(new HashMap(map));

        map.clear();
        map.put("textName", "Actualizar Pais");
        map.put("handleMethodName", "handleButtonUpdateCountry");
        map.put("layoutX", "139.0");
        list.add(new HashMap(map));

        map.clear();
        map.put("textName", "Eliminar Pais");
        map.put("handleMethodName", "handleButtonRemoveCountry");
        map.put("layoutX", "262.0");
        list.add(new HashMap(map));

        vt.addParameter("buttonList", new ArrayList(list));

        vt.doWork();
        vt.saveToFile();
    }

    private void createUpdateCountry(VelocityBuilder vt){
        URL url = MainApp.class.getClassLoader().getResource("resources/ActivityCreateUpdateView.vm");
        System.out.println(url.getFile().toString());

        // Set template to load
        vt.setTemplate("resources/ActivityCreateUpdateView.vm");

        ArrayList list = new ArrayList();
        Map map = new HashMap();

        map.put("name", "AnchorPane");
        map.put("controller", "com.client.controller.ActivityCreateUpdateController");
        vt.addParameter("class", new HashMap(map));

        map.clear();
        map.put("textName", "Nombre del Pais");
        map.put("gridColumn", "0");
        map.put("gridRow", "0");
        list.add(new HashMap(map));

        map.clear();
        map.put("textName", "Equipo de Futbol");
        map.put("gridColumn", "0");
        map.put("gridRow", "1");
        list.add(new HashMap(map));

        map.clear();
        map.put("textName", "Nombre del Presidente");
        map.put("gridColumn", "0");
        map.put("gridRow", "2");
        list.add(new HashMap(map));

        vt.addParameter("labelList", new ArrayList(list));

        list.clear();
        
        map.clear();
        map.put("id", "countryName");
        map.put("gridColumn", "1");
        map.put("gridRow", "0");
        list.add(new HashMap(map));
        
        map.clear();
        map.put("id", "futbolTeamName");
        map.put("gridColumn", "1");
        map.put("gridRow", "1");
        list.add(new HashMap(map));

        map.clear();
        map.put("id", "presidentName");
        map.put("gridColumn", "1");
        map.put("gridRow", "2");
        list.add(new HashMap(map));

        vt.addParameter("textList", new ArrayList(list));

        list.clear();

        map.clear();
        map.put("textName", "Guardar");
        map.put("handleMethodName", "handleButtonSave");
        map.put("layoutX", "77.0");
        list.add(new HashMap(map));

        map.clear();
        map.put("textName", "Cancelar");
        map.put("handleMethodName", "handleButtonCancel");
        map.put("layoutX", "179.0");
        list.add(new HashMap(map));

        vt.addParameter("buttonList", new ArrayList(list));

        vt.doWork();
        vt.saveToFile();
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }

}
