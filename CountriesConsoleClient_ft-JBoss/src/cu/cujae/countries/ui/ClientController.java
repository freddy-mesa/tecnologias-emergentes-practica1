package cu.cujae.countries.ui;

import java.rmi.RemoteException;
import java.util.Collection;
import java.util.Hashtable;

import javax.ejb.CreateException;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.rmi.PortableRemoteObject;

import org.demo.service.ejb.CountryService;
import org.demo.service.ejb.CountryServiceHome;
import org.demo.vo.CountryVO;
import org.demo.vo.FutbolTeamVO;

/**
 * @author Daniel
 *
 */
public class ClientController {

	private static ClientController clientController;
	private static Hashtable environment = new Hashtable();
	
	private ClientController() {
		super();
		environment.put(InitialContext.INITIAL_CONTEXT_FACTORY, "org.jnp.interfaces.NamingContextFactory");
        environment.put(InitialContext.URL_PKG_PREFIXES, "org.jboss.naming:org.jnp.interfaces");
        environment.put(InitialContext.PROVIDER_URL, "jnp://localhost");
	}
	
	public static ClientController getInstance(){
		if(clientController == null)
			clientController = new ClientController();
		return clientController;
	}
	
	/**
	 * Metodos para obtener los servicios remotos
	 *
	 */
	private static <T>Object lookupHome(Hashtable environment, String jndiName, Class<T> narrowTo) throws NamingException {
		Context ctx = new InitialContext(environment);
		try {
			Object reference = ctx.lookup(jndiName);
			return PortableRemoteObject.narrow(reference, narrowTo);
		} finally {
			ctx.close();
		}
	}
	private CountryService getCountryService (){
		try {
			CountryServiceHome countryServiceHome = (CountryServiceHome) lookupHome(environment, CountryServiceHome.JNDI_NAME, CountryServiceHome.class);
			return countryServiceHome.create();
		} catch (NamingException e) {
			e.printStackTrace();
		} catch (RemoteException e) {
			e.printStackTrace();
		} catch (CreateException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * Metodos invocar operaciones de los servicios
	 *
	 */
	public Collection getAllCountries () {
		try {
			return getCountryService().getAllCountries();
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}
	
	public Long createCountry (CountryVO countryVO)
	{
		try {
			Long id = getCountryService().createCountry(countryVO);
			return id;
		} catch (RemoteException e) {
			e.printStackTrace();
		}
		return null;
	}

	public void removeCountry(Long idCountry) {
		try {
			getCountryService().removeCountry(idCountry);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Collection getCountryByName(String name){
		
		try {
			return getCountryService().getCountryByName(name);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public CountryVO getCountryById(Long idCountry){
		
		try {
			return getCountryService().getCountryById(idCountry);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Long updateCountry(CountryVO countryVO)
	{
		try {
			return getCountryService().updateCountry(countryVO);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public FutbolTeamVO getFutbolTeamByCountry(CountryVO countryVO){
		try {
			return getCountryService().getFutbolTeamByCountry(countryVO);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
	public Collection getProvincesByCountry(CountryVO countryVO){
		try {
			return getCountryService().getProvincesByCountry(countryVO);
		} catch (RemoteException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}
	
}
