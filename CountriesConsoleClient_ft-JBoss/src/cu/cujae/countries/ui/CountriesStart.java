package cu.cujae.countries.ui;

import java.util.ArrayList;

import org.demo.vo.CountryVO;
import org.demo.vo.FutbolTeamVO;
import org.demo.vo.PresidentVO;

public class CountriesStart {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		ClientController clientController = ClientController.getInstance();

		PresidentVO presidentVO = new PresidentVO();
		presidentVO.setName(new String("Archavin"));

		FutbolTeamVO futbolTeamVO = new FutbolTeamVO();
		futbolTeamVO.setName("Equipo RUsia");

		CountryVO countryVO = new CountryVO();
		countryVO.setName("Rusia");

		countryVO.setPresidentVO(presidentVO);
		countryVO.setFutbolTeamVO(futbolTeamVO);

		Long idCountry = clientController.createCountry(countryVO);
		countryVO = null;

		ArrayList<CountryVO> countries = (ArrayList<CountryVO>) clientController.getAllCountries();
		for (CountryVO countryVOAux : countries) {
			System.out.println(countryVOAux.getName());
		}
	}

}
