package demolast.ui;

import java.util.ArrayList;
import java.util.Collection;

import org.demo.vo.CountryVO;
import org.demo.vo.FutbolTeamVO;
import org.demo.vo.PresidentVO;
import org.demo.vo.ProvinceVO;

public class CountriesStart {

	/**
	 * @param args
	 */
	public static void main(String[] args) {

		ClientController clientController = ClientController.getInstance();

		PresidentVO presidentVO = new PresidentVO();
		presidentVO.setName(new String("Danilo Medina"));

		FutbolTeamVO futbolTeamVO = new FutbolTeamVO();
		futbolTeamVO.setName("Equipo Dominicano");

		ArrayList<ProvinceVO> provincesVO = new ArrayList<>();

		ProvinceVO provinceVO = new ProvinceVO();
		provinceVO.setName("Santiago");
		provincesVO.add(provinceVO);

		CountryVO countryVO = new CountryVO();
		countryVO.setName("Republica Dominicana");

		countryVO.setPresidentVO(presidentVO);
		countryVO.setFutbolTeamVO(futbolTeamVO);

		provinceVO.setCountryVO(countryVO);
		countryVO.setProvinceVOs((Collection) provincesVO);

		Long idCountry = clientController.createCountry(countryVO);
		countryVO = null;

		CountryVO countryVOLoad = new CountryVO();
		countryVOLoad = clientController.getCountryById(idCountry);

		countryVOLoad.setName("United State");
		countryVOLoad.getPresidentVO().setName("Barack Obama");

		/*
		 * ArrayList<ProvinceVO> provincesVO = null; if
		 * (countryVOLoad.getProvinceVOs() != null) provincesVO =
		 * (ArrayList<ProvinceVO>) countryVOLoad .getProvinceVOs(); else
		 * provincesVO = new ArrayList<>();
		 * 
		 * ProvinceVO provinceVO = new ProvinceVO();
		 * provinceVO.setName("Santiago");
		 * provinceVO.setCountryVO(countryVOLoad); provincesVO.add(provinceVO);
		 * 
		 * countryVOLoad.setProvinceVOs((Collection) provincesVO);
		 */
		clientController.updateCountry(countryVOLoad);

		/*
		 * ArrayList<CountryVO> countries = (ArrayList<CountryVO>)
		 * clientController.getAllCountries(); for (CountryVO country :
		 * countries) { System.out.println(country.getName()); }
		 */

		clientController.removeCountry(countryVOLoad.getId());
	}

}
