package demolast.ui;

import java.util.Collection;

import org.demo.ServiceLocator;
import org.demo.vo.CountryVO;
import org.demo.vo.FutbolTeamVO;

/**
 * @author Jose Luis
 * 
 */
public class ClientController {

	private static ClientController clientController;

	private ClientController() {
		super();
	}

	public static ClientController getInstance() {
		if (clientController == null)
			clientController = new ClientController();
		return clientController;
	}

	public Collection getAllCountries() {
		return ServiceLocator.instance().getCountryService().getAllCountries();
	}

	public Long createCountry(CountryVO countryVO) {
		return ServiceLocator.instance().getCountryService()
				.createCountry(countryVO);
	}

	public void removeCountry(Long idCountry) {
		ServiceLocator.instance().getCountryService().removeCountry(idCountry);
	}

	public Collection getCountryByName(String name) {
		return ServiceLocator.instance().getCountryService()
				.getCountryByName(name);
	}

	public CountryVO getCountryById(Long idCountry) {
		return ServiceLocator.instance().getCountryService()
				.getCountryById(idCountry);
	}

	public Long updateCountry(CountryVO countryVO) {
		return ServiceLocator.instance().getCountryService()
				.updateCountry(countryVO);
	}
	
	public FutbolTeamVO getFutbolTeamByCountry(CountryVO countryVO){
		return ServiceLocator.instance().getCountryService()
				.getFutbolTeamByCountry(countryVO);
	}
	
	public Collection getProvincesByCountry(CountryVO countryVO){
		return ServiceLocator.instance().getCountryService()
				.getProvincesByCountry(countryVO);
	}

}
