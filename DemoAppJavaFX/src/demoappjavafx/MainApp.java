package demoappjavafx;

import demoappjavafx.controller.ActivityController;
import demoappjavafx.controller.CountryCreateEditController;
import demoappjavafx.model.CountryBeanVO;
import java.io.IOException;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.layout.AnchorPane;
import javafx.scene.layout.BorderPane;
import javafx.stage.Modality;
import javafx.stage.Stage;

/**
 *
 * @author Freddy Mesa
 */
public class MainApp extends Application {
    
    Stage primaryStage;
    BorderPane rootLayout;
    
    @Override
    public void start(Stage stage) throws Exception {
        primaryStage = stage;
        primaryStage.setTitle("Country CRUD");
        
        rootLayout = FXMLLoader.load(getClass().getResource("view/RootLayout.fxml"));
                
        Scene scene = new Scene(rootLayout);
        primaryStage.setScene(scene);
        
        stage.show();
        
        showActivity();
    }
    
    public void showActivity() throws Exception {
        FXMLLoader loader = new FXMLLoader(getClass().getResource("view/Activity.fxml"));
        AnchorPane page = (AnchorPane) loader.load();
        rootLayout.setCenter(page);
        
        ActivityController controller = loader.getController();
        controller.setMainApp(this);
    }
    
    public void showCreateEditCountry(boolean isCreate, CountryBeanVO countryBeanVO){
        try{
            FXMLLoader loader = new FXMLLoader(getClass().getResource("view/CountryCreateEdit.fxml"));
            AnchorPane page = (AnchorPane) loader.load();
            
            Stage dialogStage = new Stage();
            
            if(isCreate)
                dialogStage.setTitle("Crear un Pais");
            else
                dialogStage.setTitle("Editar un Pais");
                
            dialogStage.initModality(Modality.WINDOW_MODAL);
            dialogStage.initOwner(this.primaryStage);
            Scene scene = new Scene(page);
            dialogStage.setScene(scene);
            
            CountryCreateEditController controller = loader.getController();
            controller.setIsCreate(isCreate);
            controller.setDialogState(dialogStage);
            
            controller.init(countryBeanVO);          
            
            dialogStage.showAndWait();
        }
        catch(IOException ex){
            ex.printStackTrace();
        }        
    }
    
    /**
     * The main() method is ignored in correctly deployed JavaFX application.
     * main() serves only as fallback in case the application can not be
     * launched through deployment artifacts, e.g., in IDEs with limited FX
     * support. NetBeans ignores main().
     *
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        launch(args);
    }

    public Stage getPrimaryStage() {
        return primaryStage;
    }
    
    
}
